package com.facebooklogin.fbloginsample;

import android.content.Intent;
import android.graphics.Bitmap;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;

import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A placeholder fragment containing a simple view.
 */

public class MainFragment extends Fragment {
    public static boolean DEBUG = false;
    //커스텀 로그인 버튼
    //Butter Knife 적용, id이용한 뷰들 연결
    @BindView(R.id.fb)
    Button fb;
    @BindView(R.id.login_button)
    LoginButton loginButton;
    @BindView(R.id.text_details)
    TextView mTextDeatils;
    @BindView(R.id.profileImage)
    ImageView mProfileImage;
    private Bitmap fbProfImgBit;
    private CallbackManager mCallbackManager;
    private URLFetchTask mTask;
    private FBCallback mFBCallback;
    //기본 페이스북 로그인 버튼
    private FacebookCallback<LoginResult> mCallback = new FacebookCallback<LoginResult>() {

        @Override
        public void onSuccess(LoginResult loginResult) {
            AccessToken accessToken = loginResult.getAccessToken();
            Log.d("Debug","AccessToken-"+accessToken.getToken());
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError(FacebookException error) {

        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
        mCallbackManager = CallbackManager.Factory.create();
        Log.d("Debug","mCallbackManager-"+mCallback.toString());
        //AccessTokenTracker
        AccessTokenTracker tracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken newAccessToken) {

            }
        };
        //ProfileTracker
        ProfileTracker profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {
                //  텍스트 변환
                displayWelcomeMessage(newProfile);
                // 프로필 이미지 및 버튼 텍스트 변환
                displayProfileImage(newProfile);
            }
        };
        tracker.startTracking();
        Log.d("Debug","accessTokenTracker-"+tracker.toString());
        profileTracker.startTracking();
        Log.d("Debug","profileTracker-"+profileTracker.toString());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Butter Knife 뷰 바인딩
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_main, null);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    //프로필 이름 set
    private void displayWelcomeMessage(Profile profile) {
        String message = (profile != null) ? "Welcome! " + profile.getName() : "Please Login";
        mTextDeatils.setText(message);
        Log.d("Debug",message);
    }

    //프로필 이미지 set
    private void displayProfileImage(Profile profile) {
        if (profile != null) {//로그인상태
            //asynctask
            startURLFetch(profile);
            // 버튼 텍스트 변경
            fb.setText("로그아웃");
            Log.d("Debug",fb.getText().toString());
        } else {//로그인 상태가 아닌경우
            mProfileImage.setImageResource(R.drawable.com_facebook_profile_picture_blank_portrait);
            fb.setText("로그인");
            Log.d("Debug",fb.getText().toString());
        }
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loginButton.setReadPermissions("user_friends");
        loginButton.setFragment(this);
        loginButton.registerCallback(mCallbackManager, mCallback);
    }

    //커스텀 로그인 버튼 클릭
    @OnClick(R.id.fb)
    void fbClick(Button button) {
        //커스텀 로그인 버튼 클릭시 기본 페이스북로그인 버튼 누른 효과가 나게함. 현재 facebook버튼은 gone상태.
        loginButton.performClick();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    //AsyncTask
    void startURLFetch(Profile profile) {
        mFBCallback = new FBCallback() {
            @Override
            public void callback() {
                //콜백메서드 할일 구현 : 비트맵 전달과 뷰에 비트맵등록
                Log.d("Debug", "콜백구현");
                fbProfImgBit = mTask.fbProfImgBitIMG;
                Log.d("Debug", fbProfImgBit.toString());
                showProfileImage();
            }
        };
        mTask = new URLFetchTask(mFBCallback);
        Log.d("Debug",mFBCallback.toString());
        mTask.execute(profile.getId());
        Log.d("Debug",profile.getId());
    }

    //set ProfileImage
    public void showProfileImage() {
        mProfileImage.setImageBitmap(fbProfImgBit);
        Log.d("Debug",fbProfImgBit.toString());
    }
}
