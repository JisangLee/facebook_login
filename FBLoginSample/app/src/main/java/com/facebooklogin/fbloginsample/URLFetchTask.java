package com.facebooklogin.fbloginsample;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import java.net.URL;

/**
 * Created by 지상 on 2016-08-16.
 */
public class URLFetchTask extends AsyncTask<String, URL, Bitmap> {
    URL fbProfImgUrl;
    Bitmap fbProfImgBitIMG;
    String data;
    private FBCallback mFBCallback;

    //URLFetchTask생성자 Callback초기화
    public URLFetchTask(FBCallback callback) {
        this.mFBCallback =callback;
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        try {
            data = params[0];
            Log.d("Debug","param[0]-" + data);
            // Background 단 처리 (URL 및 Bitmap처리)
            fbProfImgUrl = new URL("https://graph.facebook.com/" + data + "/picture?type=large");
            Log.d("Debug","fbProfImgUrl-"+fbProfImgUrl.toString());
            fbProfImgBitIMG = BitmapFactory.decodeStream(fbProfImgUrl.openConnection().getInputStream());
            Log.d("Debug","fbProfImgBitIMG-"+fbProfImgBitIMG.toString());
        } catch (Exception ex) {

        }

        return fbProfImgBitIMG;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        mFBCallback.callback();
        Log.d("Debug","onPostExcute-" + mFBCallback.toString());
    }
}


